add_executable(numerics_check EXCLUDE_FROM_ALL numerics_check.f90)
target_include_directories(numerics_check PRIVATE ${CMAKE_BINARY_DIR}/src/include ${FUTILE_INCLUDE_DIRS})
target_link_libraries(numerics_check PRIVATE ${FUTILE_LINK_LIBRARIES} atlab ${OpenMP_Fortran_LIBRARIES})
set_target_properties(numerics_check PROPERTIES LINKER_LANGUAGE Fortran)
if(BUILD_OPENMP AND OpenMP_FOUND)
  target_compile_options(numerics_check PUBLIC ${OpenMP_Fortran_FLAGS})
  # Available in 3.12, so OpenMP libraries can be removed.
  # target_link_options(numerics_check PUBLIC ${OpenMP_Fortran_FLAGS})
endif()
add_custom_command(OUTPUT numerics_check.log domain.yaml
  COMMAND cp -f ${CMAKE_CURRENT_SOURCE_DIR}/domain.yaml ${CMAKE_CURRENT_BINARY_DIR}
  COMMAND ./numerics_check > numerics_check.log
  DEPENDS domain.yaml numerics_check
  COMMENT "Running numerics_check...")
set(TESTS numerics_check)
  
if (BABEL_FOUND)
  add_executable(babel EXCLUDE_FROM_ALL babel.f90)
  target_include_directories(babel PRIVATE ${CMAKE_BINARY_DIR}/src/include ${FUTILE_INCLUDE_DIRS})
  target_link_libraries(babel PRIVATE ${FUTILE_LINK_LIBRARIES} atlab)
  set_target_properties(babel PROPERTIES LINKER_LANGUAGE Fortran)
  add_custom_command(OUTPUT babel.log outfile.xyz OpenBabel.err
    COMMAND ./babel -i ${CMAKE_CURRENT_SOURCE_DIR}/TiO2.cif > babel.log
    DEPENDS TiO2.cif babel
    COMMENT "Running babel...")
  list(APPEND TESTS babel)
endif()

set(LOGS)
set(REPORTS)
foreach(test ${TESTS})
  list(APPEND LOGS ${test}.log)
  list(APPEND REPORTS ${test}.report.yaml)
endforeach(test)
add_custom_target(check
  for test in ${TESTS}\; do
    ${PYTHON_EXECUTABLE} ${FUTILE_PYTHONDIR}/fldiff_yaml.py
    -r ${CMAKE_CURRENT_SOURCE_DIR}/$$\{test\}.ref.yaml
    -d $$\{test\}.log
    -t ${CMAKE_CURRENT_SOURCE_DIR}/tols.yaml
    --label=$$\{test\} -o $$\{test\}.report.yaml\;
  done
  DEPENDS tols.yaml numerics_check.ref.yaml babel.ref.yaml ${LOGS}
  BYPRODUCTS report_input report_remarks ${REPORTS})
add_custom_command(TARGET check POST_BUILD
  COMMAND ${PYTHON_EXECUTABLE} ARGS ${FUTILE_PYTHONDIR}/report.py)
