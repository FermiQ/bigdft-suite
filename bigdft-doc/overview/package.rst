The BigDFT-suite Package
========================

BigDFT is a suite containing many different programs, all of which are
maintained on our
`gitlab page <http://https://gitlab.com/l_sim/bigdft-suite/>`_ 
Instead of building the code with one single configure line, the code is
built as a suite of different packages.

.. figure:: /static/buildprocedure.png
   :scale: 50 %
   :alt: buildprocedure.png
   :align: left

In this scheme we might see how the BigDFT code itself is separated. The figure
describes the interdependencies among these packages. Let us describe each of
the packages which are depicted here. The packages might be separated in to
*upstream* contributions and *native* contributions. There are also further
python packages which can optionally be included.

Upstream packages
-----------------

-  ``libyaml``: this library is used to parse the
   `yaml markup language <http://yaml.org/>`_ that is used in BigDFT input
   files;
-  ``PyYaml``: this is a `Python module <https://pyyaml.org/>`_ which makes it
   possible to convert yaml into python objects. This part is mainly
   used for postprocessing purposes as BigDFT logfile also comes in yaml
   format;
-  ``libXC``:
   this `library <http://www.tddft.org/programs/octopus/wiki/index.php/Libxc>`_
   handles most of the XC functionals that can be invoked from
   BigDFT runs;
-  ``GaIn``: this library handles analytic integrals of common operators
   between Gaussian Functions. It does not perform low-level operations
   and can be linked separately;

Native packages
---------------

-  :ref:`futile <futile:futile_index>`:  a library
   handling most common FORTRAN low-level operations like memory
   management, profiling routines, I/O operations. It also supports yaml
   output and parsing for fortran programs. It further provides wrappers
   routines to MPI and linear algebra operations. This library is
   extensively used in BigDFT packages;
-  ``CheSS``: A module for performing Fermi Operator Expansions via
   Chebyshev Polynomials, released as a separate project on
   `Launchpad <https://launchpad.net/chess>`_
-  ``psolver``: a flexible real-space Poisson Solver based on
   Interpolating Scaling Functions. It constitutes a fundamental
   building block of BigDFT code, and it can also be used separately and
   linked to other codes. It also internally uses the ``futile`` library
   for the I/O.
-  ``libABINIT``: this is a subsection of files coming from
   `ABINIT <http://www.abinit.org>`_ software package, to which
   BigDFT has been coupled since the early days. It handles different
   parts like symmetries, ewald corrections, PAW routines, density and
   potential mixing routines and some MD minimizers. Also some XC
   functionals, initially natively implemented in the ``ABINIT`` code,
   are also coded in this library. This library uses the ``futile``
   code through the (experimental) PAW section.
-  `BigDFT <http://www.bigdft.org>`_: the core routines for computing
   electronic structure;
-  ``spred``: a library for structure prediction tools that is compiled
   on top of BigDFT routines.

.. todo:: List the optional packages (openbabel, etc)

In the older versions of the code, all these different packages were compiled
with the same configuration instructions. With the present version, each of
the code sections described above can be considered as a separate
package (some more are upcoming), which improves modularity between code
sections and reduces side-effects. In addition, each package can now be
compiled with different installation instructions.

We are using a build suite tool based on the
`Jhbuild <https://wiki.gnome.org/action/show/Projects/Jhbuild?action=show&redirect=Jhbuild>`_ project
which is regularly employed by developers of ``gnome`` project. We have
re-adapted/added some of the functionality of the ``jhbuild`` package to
meet the needs of our package.