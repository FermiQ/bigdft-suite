License
=======

BigDFT-suite is a free and open source collection of software packages.
Each of the different packages have their own associated license. The most
common is the lesser gnu public license version 2.

.. todo:: describe the licenses of each package in BigDFT?