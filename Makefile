
        #This Makefile is automatically generated from the BigDFT installer to avoid the calling to
        #the installer again for future action on this build
        #Clearly such actions only _assume_ that the build is fully functional and almost nothing
        #can be done with this file if a problem might arise.
        #Otherwise stated: this is an automatic message, please do not reply.
all: build
        
startover:  
	/home/sp2m_l_sim/bigdft-suite/Installer.py startover spred -y
dist:  
	/home/sp2m_l_sim/bigdft-suite/Installer.py dist spred -y
autogen:  
	/home/sp2m_l_sim/bigdft-suite/Installer.py autogen spred -y
cleanone:  
	/home/sp2m_l_sim/bigdft-suite/Installer.py cleanone spred -y
update:  
	/home/sp2m_l_sim/bigdft-suite/Installer.py update spred -y
buildone:  
	/home/sp2m_l_sim/bigdft-suite/Installer.py buildone spred -y
check:  
	/home/sp2m_l_sim/bigdft-suite/Installer.py check spred -y
dry_run:  
	/home/sp2m_l_sim/bigdft-suite/Installer.py dry_run spred -y
make:  
	/home/sp2m_l_sim/bigdft-suite/Installer.py make spred -y
build:  
	/home/sp2m_l_sim/bigdft-suite/Installer.py build spred -y
clean:  
	/home/sp2m_l_sim/bigdft-suite/Installer.py clean spred -y
link:  
	/home/sp2m_l_sim/bigdft-suite/Installer.py link spred -y
