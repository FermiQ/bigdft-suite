!> @file
!!   Basic routines for i/o based on ntpoly.
!! @author
!!   Copyright (C) 2016 CheSS developers
!!
!!   This file is part of CheSS.
!!   
!!   CheSS is free software: you can redistribute it and/or modify
!!   it under the terms of the GNU Lesser General Public License as published by
!!   the Free Software Foundation, either version 3 of the License, or
!!   (at your option) any later version.
!!   
!!   CheSS is distributed in the hope that it will be useful,
!!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!!   GNU Lesser General Public License for more details.
!!   
!!   You should have received a copy of the GNU Lesser General Public License
!!   along with CheSS.  If not, see <http://www.gnu.org/licenses/>.


!> Basic routines for i/o based on ntpoly.
module bigpoly_io
  use futile
  use processgridmodule, only : constructprocessgrid, destructprocessgrid
  use psmatrixmodule, only : matrix_ps, writematrixtomatrixmarket, &
     writematrixtobinary, destructmatrix
  use sparsematrix_base, only : sparse_matrix, matrices
  implicit none
  private

  !> Public routines
  public :: bigpoly_write_matrix_mtx
  public :: bigpoly_write_matrix_binary

  contains

    !> Write a matrix to matrix market.
    subroutine bigpoly_write_matrix_mtx(inmat, inval, filename, comm)
      !> Input CheSS sparse matrix type
      type(sparse_matrix), intent(in):: inmat
      !> Input CheSS matrix of data.
      type(matrices), intent(in) :: inval
      !> The name of the file to write to.
      character(len=*), intent(in) :: filename
      !> The communicator to distribute along.
      integer, intent(in) :: comm
      ! Local variables
      type(matrix_ps) :: ntinmat

      call f_routine(id='write_matrix_mtx')

      ! Create the process grid
      call constructprocessgrid(comm)

      ! Conversion of input matrix.
      call chess_to_ntpoly(inmat, inval, ntinmat)

      ! Write
      call writematrixtomatrixmarket(ntinmat, filename)

      ! Cleanup
      call destructmatrix(ntinmat)
      call destructprocessgrid()

      call f_release_routine()
    end subroutine bigpoly_write_matrix_mtx

    !> Write a matrix to binary.
    subroutine bigpoly_write_matrix_binary(inmat, inval, filename, comm)
      !> Input CheSS sparse matrix type
      type(sparse_matrix), intent(in):: inmat
      !> Input CheSS matrix of data.
      type(matrices), intent(in) :: inval
      !> The name of the file to write to.
      character(len=*), intent(in) :: filename
      !> The communicator to distribute along.
      integer, intent(in) :: comm
      ! Local variables
      type(matrix_ps) :: ntinmat

      call f_routine(id='write_matrix_mtx')

      ! Create the process grid
      call constructprocessgrid(comm)

      ! Conversion of input matrix.
      call chess_to_ntpoly(inmat, inval, ntinmat)

      ! Write
      call writematrixtobinary(ntinmat, filename)

      ! Cleanup
      call destructmatrix(ntinmat)
      call destructprocessgrid()

      call f_release_routine()
    end subroutine bigpoly_write_matrix_binary

end module bigpoly_io
